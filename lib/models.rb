require 'data_mapper'

# to be quite honest I /do/ want this to be a little over-engineered,
# because I'd prefer it to have too much stuff thats never used than
# to not have any of it in the model and then have to redesign large
# parts later.
module Ficdb
  class Book
    include DataMapper::Resource
    property :id, Serial
    property :title, String
    property :summary, Text
    
    # associations to other models
    belongs_to :author
    has n, :taggings
    has n, :tags, :through => :taggings
    
  # TODO: write something so we can track edits.
  end
  
  class Trope
    include DataMapper::Resource
    property :id, Serial
    property :name, String
    property :desc, Text
    # TODO: write something so we can track edits.
  end
  
  
  class TropeSpoiler 
    include DataMapper::Resource
    property :id, Serial
    belongs_to :book
    belongs_to :trope
    property :spoiler, Text
    # TODO: write something so we can track edits.
  end 
  
  class Author
    include DataMapper::Resource
    property :id, Serial
    property :name, String
    property :bio, Text
    has n, :books
    # TODO: write something so we can track edits.
  end
   
  class Tagging
    include DataMapper::Resource
    belongs_to :tag, :key => true
    belongs_to :book, :key => true
  end
  
  class Book
    include DataMapper::Resource
    property :id, Serial
    property :title, String
    property :summary, Text
    
    # associations to other models
    belongs_to :author
    has n, :taggings
    has n, :tags, :through => :taggings
    
    # TODO: write something so we can track edits.
  end
  
  class Tag
    include DataMapper::Resource
    property :id, Serial
    property :tag, String  
    has n, :taggings
    has n, :books, :through => :taggings
  end

  class EditLog
    include DataMapper::Resource
    
    property :id, Serial
    property :type, Integer  # 0 is creation, 1 is modified, 2 is deletion
    # association for who made this.
    belongs_to :author, :model => 'User'
    property :time, DateTime
    property :minor, Boolean, :default => true
    property :desc, Text     # 
    property :diff, Text     # content of 
  end

  class User
    include DataMapper::Resource
    property :id, Serial
    property :username, String
    property :password, String
    property :email, String
    property :can_login, Boolean
    property :editor, Boolean
    property :admin, Boolean
    property :joined, DateTime
    property :modified, DateTime
  end
  
  DataMapper.finalize
end

